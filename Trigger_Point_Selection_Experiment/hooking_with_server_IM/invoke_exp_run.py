import frida
import sys
import time
import subprocess 
import os
import signal
from datetime import datetime

procname='org.telegram.messenger'
test_name=""

hook_list=[

# 'cp|appsp|org.telegram.ui.Components.ChatActivityEnterView.sendMessage',
# 'cp|appsp|org.telegram.ui.Components.ChatActivityEnterView.sendMessageInternal',
# 'cp|appsp|org.telegram.ui.Components.ChatActivityEnterView.processSendingText',
# 'cp|appsp|org.telegram.messenger.SendMessagesHelper.sendMessage',
# 'cp|appsp|org.telegram.ui.Cells.DialogCell.update',
# 'cp|appsp|org.telegram.ui.DialogsActivity.updateVisibleRows', 
# 'cp|appsp|org.telegram.tgnet.ConnectionsManager.native_sendRequest',
# 'cp|androidsp|android.widget.TextView.setText',
# 'cp|androidsp|android.os.Handler.sendMessageAtTime',
# 'cp|native|recv',
# 'cp|native|send',

# 'cp|appsp|org.telegram.messenger.SendMessagesHelper.performSendMessageRequest',
# 'cp|appsp|org.telegram.tgnet.ConnectionsManager.native_sendRequest',
# 'cp|androidsp|android.app.SharedPreferencesImpl$EditorImpl.commitToMemory',
# 'cp|androidsp|android.widget.EditText.setText',
'cp|native|open',
# 'cp|native|send',
# 'cp|device|network',
# 'cp|device|file',
# 'spying|appsp|org.telegram.messenger.MessagesStorage.putMessages',
# 'spying|appsp|org.telegram.ui.Cells.DialogCell.update',
# 'spying|androidsp|android.app.ContextImpl.sendBroadcast',
# 'spying|androidsp|android.view.ViewGroup.dispatchGetDisplayList',
# 'spying|native|open',
# 'spying|native|recv',
# 'spying|device|network',
# 'spying|device|file',

# 'spying|appsp|org.telegram.ui.DialogsActivity.didReceivedNotification',
# 'spying|appsp|org.telegram.messenger.NotificationCenter.postNotificationNameInternal',
# 'spying|appsp|org.telegram.ui.DialogsActivity.updateVisibleRows',
# 'spying|appsp|org.telegram.ui.Cells.DialogCell.checkCurrentDialogIndex',
# 'spying|appsp|org.telegram.messenger.MessagesController.processUpdates',
# 'spying|appsp|org.telegram.messenger.NotificationsController.showOrUpdateNotification',
# 'spying|androidsp|android.os.MessageQueue.next',
# 'spying|native|recv',
# 'spying|native|send',

]


filename="hook_template.js"
native_filename="hook_native.js"
message="Sending%stext%smessage%s" 
hook_invoke=''
deviceUT='emulator-5554'
externalDevice='96RAY0YTEX'

def check_frida():
    o=subprocess.Popen(['frida-ps', '-D', 'emulator-5554'], stdout=subprocess.PIPE)
    out, _ = o.communicate()
    if out=='Failed to enumerate processes: unable to connect to remote frida-server: closed':
        subprocess.Popen(['sudo', '/root/Android/Sdk/platform-tools/adb', '-s', 'emulator-5554', 'shell', '"/data/local/tmp/frida-server &"'], stdout=outfile, stderr=outfile)

if len(sys.argv) > 1:
    counter = int(sys.argv[1])
    test_name =  str(sys.argv[2])
else:
    counter=0

for count in range(8,9):
    for hook in hook_list:
        now = datetime.now()
        print("** hook: "+hook+" ** start: "+ now.strftime("%m/%d/%Y, %H:%M:%S")+" **")
    
        counter+=1
        var=hook.split('|')
        scenario = var[0]
        category = var[1]
        method = var[2]
        
        dir_name='results/'+test_name+str(count)+'/'+scenario+"_"+category+"_"+method
        subprocess.call(['mkdir', '-p', dir_name])
        scriptname=dir_name+"/hook.js"
        
        subprocess.call(['sudo', '/root/Android/Sdk/platform-tools/adb', '-s',deviceUT,'shell', 'am', 'force-stop',procname])
        time.sleep(2)
        subprocess.call(['sudo','monkeyrunner/launch_app.sh', deviceUT])      
        hook_invoke = 'hook_template_heavy.py'
        scriptToRun='adb_monkey_runner_6.1.1_minified.sh'

        if(scenario == "spying") :
            toReplace="traceReceiveMethod('org.telegram.messenger.MessagesController.processLoadedMessages');"
        else:
            toReplace=''
        
        if category =='native':
            with open(native_filename, "rt") as fin:
                with open(scriptname, "w") as fout:
                    for line in fin:
                        fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category+"_"+method).replace('[FUNCTION]', method).replace('"[TRACE_RECEIVE]"',toReplace))
        elif category =='device':
            with open("hook_"+category+"_"+method+".js", "rt") as fin:
                with open(scriptname, "w") as fout:
                    for line in fin:
                        fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category+"_"+method).replace('"[TRACE_RECEIVE]"',toReplace).replace('"[SENDING]"', "true" if scenario == 'cp' else "false"))
        else:
            with open(filename, "rt") as fin:
                with open(scriptname, "w") as fout:
                    for line in fin:
                        fout.write(line.replace('[SCENARIO]', scenario).replace('[TYPE]', category).replace('[TRACE_METHOD]', method).replace('"[TRACE_RECEIVE]"',toReplace))

        messageToSend = message+str(counter)

        if scenario=="spying": # execute commands on other emulator to send. First emulator to open must be the receiver
            messageToSend = messageToSend.replace("Sending", "Receiving")
            subprocess.call(['sudo','./monkeyrunner/adb_monkey_runner_6.1.1_minified_Pixel.sh', dir_name, messageToSend, '96RAY0YTEX'])
            time.sleep(10)
            
            print("*** CALLING PYTHON *** >>> "+scenario+"  |  "+category+"  |  "+method)
            outfile = open('stdout','w')
            p = subprocess.Popen(['python3', hook_invoke, scriptname, procname],  stdout=outfile, stderr=outfile) 
            time.sleep(5)
            scriptToRun='load_screen.sh'
            print("*** CALLING *** >>> `sudo ./post_exec.sh "+dir_name+" "+messageToSend+" "+scriptToRun+" "+deviceUT+" "+deviceUT+"`")
            subprocess.call(['sudo','./post_exec.sh', dir_name, messageToSend, scriptToRun, deviceUT, deviceUT])
        else:
            print("*** CALLING PYTHON *** >>> "+scenario+"  |  "+category+"  |  "+method)
            outfile = open('stdout','w')
            p = subprocess.Popen(['python3', hook_invoke, scriptname, procname],  stdout=outfile, stderr=outfile) 
            time.sleep(5)

            print("Calling `sudo ./post_exec.sh "+dir_name+" "+messageToSend+"`")
            subprocess.call(['sudo','./post_exec.sh', dir_name, messageToSend, scriptToRun, deviceUT, deviceUT])
            #pulling evidence for time received
            subprocess.call(['sudo', '/root/Android/Sdk/platform-tools/adb', '-s',externalDevice,'pull', '/sdcard/telegram_jitmflogs/received_log.log',dir_name])
            subprocess.call(['sudo', '/root/Android/Sdk/platform-tools/adb', '-s',externalDevice, 'shell','rm', '-rf', '/sdcard/telegram_jitmflogs/received_log.log'])

        pid = p.pid
        os.kill(pid, signal.SIGINT)

        time.sleep(5)

        now = datetime.now()
        print("end: ", now)
    print("end round "+str(count)+".")
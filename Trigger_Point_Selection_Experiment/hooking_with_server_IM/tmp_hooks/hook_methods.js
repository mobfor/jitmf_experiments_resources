"use strict";

var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";
var time = 0;


function traceMethod(targetClassMethod) {
	var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

	var hook = Java.use(targetClass);

	var overloadCount = 0
	try {
		overloadCount = hook[targetMethod].overloads.length;
	}
	catch (err) { }
	console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
	Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");

	for (var i = 0; i < overloadCount; i++) {
		var test = hook[targetClassMethod]
		hook[targetMethod].overloads[i].implementation = function () {

			Java.perform(function () {
				console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
			});

			var out = '{"Times": ' + (time + 1).toString() + ', "method": ' + targetClassMethod + ','

			for (var j = 0; j < arguments.length; j++) {
				var term_char = ', '
				var arg = arguments[j];

				out += '"arg[' + j + ']": ' + arguments[j] + term_char

			}
			var t = false
			Java.choose("org.telegram.messenger.MessageObject", {
				onMatch: function (instance) {
					// if (!t){
					var r = Java.cast(instance, Java.use("org.telegram.messenger.MessageObject"))
					console.log(r.localName.value)
					console.log(r.dateKey.value)
					console.log(r.messageText.toString().value)


					var CharSequence = Java.use("java.lang.CharSequence");

					// EditText.getText.overload().implementation = function () {
					// retval = this.getText.call(this);
					var text = Java.cast(r.messageText.value, CharSequence);
					console.log("[*] EditText Return: " + text);
					// };

					// t = true
					// }
				}, onComplete: function () {
				}
			});

			// if (j===1){    //performSendMessageRequest arg1 is  MessageObject
			// 	var MO = Java.use('org.telegram.messenger.MessageObject')
			// 		var ret = Java.cast(arguments[j],MO)
			// 		console.log(ret.toString())
			// }






			var retval = this[targetMethod].apply(this, arguments);
			out += '"retval": ' + retval + '}\n'
			console.error(out);
			Log.v(TAG_L, out);
			return retval;
		}
	}
}
setTimeout(function () {

	Java.perform(function () {

		// ---------------------   IM Send  ---------------------------------------------------
		// traceMethod('org.telegram.ui.Cells.DialogCell.update')
		// traceMethod('org.telegram.ui.Components.ChatActivityEnterView.sendMessage')
		// traceMethod('org.telegram.ui.Components.ChatActivityEnterView.sendMessageInternal')
		// traceMethod('org.telegram.ui.Components.ChatActivityEnterView.processSendingText')
		// traceMethod('org.telegram.messenger.SendMessagesHelper.sendMessage')
		// traceMethod('org.telegram.messenger.NotificationCenter.postNotificationName')
		// traceMethod('org.telegram.ui.DialogsActivity.updateVisibleRows')
		// traceMethod('org.telegram.messenger.MessagesStorage.putMessagesInternal')
		traceMethod('org.telegram.SQLite.SQLiteDatabase.executeFast')
		// traceMethod('org.telegram.messenger.SendMessagesHelper.performSendMessageRequest') //messageobject in param

		// ----- Android -----

		// traceMethod('android.app.SharedPreferencesImpl$EditorImpl.commitToMemory')
		// traceMethod('android.widget.TextView.setText')
		// traceMethod('android.widget.EditText.setText')
		// traceMethod('android.os.Handler.sendMessageAtTime')



		// alot --Trace('org.telegram.messenger.SecretChatHelper') //receive

		// ---------------------   IM Receive  ----------------------------------------------------		
		// traceMethod('org.telegram.ui.DialogsActivity.didReceivedNotification')
		// traceMethod('org.telegram.messenger.NotificationCenter.postNotificationNameInternal')
		// traceMethod('org.telegram.ui.DialogsActivity.updateVisibleRows')
		// traceMethod('org.telegram.ui.Cells.DialogCell.update')
		// traceMethod('org.telegram.ui.Cells.DialogCell.checkCurrentDialogIndex'
		// traceMethod('org.telegram.messenger.MessagesController.processUpdates')
		// traceMethod('org.telegram.messenger.NotificationsController.showOrUpdateNotification')

		// ----- Android -----
		// traceMethod('android.app.ContextImpl.sendBroadcast') //android.intent.action.BADGE_COUNT_UPDATE
		// traceMethod('android.view.ViewGroup.dispatchGetDisplayList') 
		// traceMethod('android.os.MessageQueue.next') 



	})
}, 0);
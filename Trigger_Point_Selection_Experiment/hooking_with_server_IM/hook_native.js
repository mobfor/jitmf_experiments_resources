var LOG_DIR = "telegram_jitmflogs";
var APP = "telegram";
var SCENARIO = "[SCENARIO]";
var TYPE = "[TYPE]";

var jitmfHeapLog = ''
var jitmfLog = ''
var online=false
var complete=false
if(online){
    complete = false
}else{
    complete = true
}

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getUnixTime(){
	return Math.round(new Date().getTime()/1000);
}

function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    var file = new File(fileName, mode);
    file.write(contents)
    file.close()
}

function dumpHeap() {
    var memdump_filename = APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": ' + getTime() + ', "method": ' + TYPE + ', "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function parseObject(instance) {
    var object = ''
    try {
        var r = Java.cast(instance, Java.use("org.telegram.messenger.MessageObject"))
        var CharSequence = Java.use("java.lang.CharSequence");
        var text = Java.cast(r.messageText.value, CharSequence);

        var Message = Java.use("org.telegram.tgnet.TLRPC$Message");
        var messageOwner = Java.cast(r.messageOwner.value, Message);

        var Peer = Java.use("org.telegram.tgnet.TLRPC$TL_peerUser");
        var user = Java.cast(messageOwner.to_id.value, Peer);

        object = '{"date": ' + messageOwner.date.value + ', "message_id": ' + messageOwner.id.value + ', "text": ' + text + ', "to": ' + user.user_id.value + '}'

    } catch (err) { }
    return object
}


function getMessageObjects() {
    var time = getTime()

    var klass = "org.telegram.messenger.MessageObject"
    Java.choose(klass, {
        onMatch: function (instance) {
            var object = parseObject(instance)
            if (object != '') {
                jitmfLog += '{"time": ' + time + ', "method": ' + TYPE + ', "object": '
                jitmfLog += object + '}\n'
            }

        }, onComplete: function () {
            complete = true
        }
    });
}
function traceReceiveMethod(targetClassMethod) {
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;
	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)
	var hook = Java.use(targetClass);
	var overloadCount = 0

	var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/received_log.log"
	try {
		overloadCount = hook[targetMethod].overloads.length;
	} catch (err) { }

	for (var i = 0; i < overloadCount; i++) {
		hook[targetMethod].overloads[i].implementation = function () {
			var time = getTime()
			var hookLog = getUnixTime() + ', Messages populated\n'
			putInFile(hookLogName, hookLog)
			var retval = this[targetMethod].apply(this, arguments);
			return retval;
		}
	}
}

setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".jitmflog";
        "[TRACE_RECEIVE]"
        var found = false;
        if ('[FUNCTION]' == 'open') {
            Interceptor.attach(Module.getExportByName('libc.so', '[FUNCTION]'), {
                onLeave: function (args) {
                    var time = getTime()
                    var hookLog = time + ',' + SCENARIO + ',' + TYPE + '\n'
                    if(online){
                        getMessageObjects(SCENARIO)
                    } else {
                        dumpHeap(SCENARIO)
                    }
                    if (complete) {
                        putInFile(hookLogName, hookLog)
                        if(online){
                            putInFile(jitmfLogName, jitmfLog)
                        } else {
                            putInFile(jitmfHeapLogName, jitmfHeapLog)
                        }
                    }    
                    jitmfHeapLog = ''
                    jitmfLog = ''               
                }
            });
        }
        else {
            Interceptor.attach(Module.getExportByName('libc.so', '[FUNCTION]'), {
                onEnter: function (args) {
                    var fd = args[0].toInt32();

                    if (Socket.type(fd) === null)
                        return;
                    if (Socket.type(fd).toString().indexOf('tcp') > -1) {
                        var address = Socket.peerAddress(fd);
                        if (address === null)
                            return;
                        found = true
                    }
                },
                onLeave: function (args) {
                    if (found) {
                        var time = getTime()
                        var hookLog = time + ',' + SCENARIO + ',' + TYPE + '\n'
                        if(online){
                            getMessageObjects(SCENARIO)
                        } else {
                            dumpHeap(SCENARIO)
                        }
                        if (complete) {
                            putInFile(hookLogName, hookLog)
                            if(online){
                                putInFile(jitmfLogName, jitmfLog)
                            } else {
                                putInFile(jitmfHeapLogName, jitmfHeapLog)
                            }
                        }
                    }
                    jitmfHeapLog = ''
                    jitmfLog = ''
                }
            });
        }
    });
});
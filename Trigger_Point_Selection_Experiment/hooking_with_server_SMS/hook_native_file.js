var LOG_DIR = "pushbullet_jitmflogs";
var APP = "pushbullet";
var SCENARIO = "[SCENARIO]";
var TYPE = "[TYPE]";

var jitmfHeapLog = ''
var jitmfLog = ''

var online=true
var complete=false
if(online){
    complete = false
}else{
    complete = true
}

function getTime(file) {
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;
	if (file) {
		dateTime = date + '_' + time;
	}
	return dateTime
}

function getExternalStorageDirectory() {
	const Env = Java.use("android.os.Environment")
	var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
	return external_storage_dir
}

function putInFile(fileName, contents) {
    var mode = 'a+';
    var file = new File(fileName, mode);
    file.write(contents)
    file.close()
}

function dumpHeap(targetClassMethod) {
    var memdump_filename = APP + '_' + SCENARIO + "_" + TYPE + "_" + targetClassMethod + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": ' + getTime() + ', "method": ' + targetClassMethod + ', "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function checkForMatch(str){
    var json=str.toString()
    var toReturn=false;
    if(SCENARIO=="spying"){
        var str1='{"type":"push","push":{"type":"sms_changed","source_device_iden":.*'
        var str2='{"key":.*"type":"sms".*"body":.*}'
        var str3='{"type":"push","targets":\\["stream","android","ios"\\],"push":{"type":"sms_changed".*'
    
        var re1 = new RegExp(str1,'g')
        var re2 = new RegExp(str2,'g')
        var re3 = new RegExp(str3,'g')

        var res1 = json.match(re1);
        var res2 = json.match(re2);
        var res3 = json.match(re3);
        
        if((res1!==null)||(res2!==null)||(res3!==null)){
            toReturn=true
        }    
    }
    else{
        var str1='{"active":.*"message":.*}}'
        var str2='{"type":"push","targets":\\["stream","android","ios"\\],"push":{"conversation_iden":.*'
        var str3='{"key":.*"type":"sms".*"body":.*}'
        var str4='{"type":"push","push":{"type":"sms_changed","source_device_iden":.*}}'

        var re1 = new RegExp(str1,'g')
        var re2 = new RegExp(str2,'g')
        var re3 = new RegExp(str3,'g')
        var re4 = new RegExp(str4,'g')

        var res1 = json.match(re1);
        var res2 = json.match(re2);
        var res3 = json.match(re3);
        var res4 = json.match(re4);
        
        if((res1!==null)||(res2!==null)||(res3!==null)||(res4!==null)){
            toReturn=true
        }
    }
    return toReturn
}

function getMessageObjects() {
    var time = getTime()

    var klass = "org.json.JSONObject"
    Java.choose(klass, {
        onMatch: function (instance) {
            var object = instance
            if (object != '') {
				if(checkForMatch(object)){
					jitmfLog += '{"time": ' + time + ', "method": ' + TYPE + ', "object": '
					jitmfLog += object + '}\n'
				}
            }

        }, onComplete: function () {
            complete = true
        }
    });
}

setImmediate(function () {
	Java.perform(function () {
		var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".hooklog"
		var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".jitmfheaplog"
		var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + '_' + SCENARIO + "_" + TYPE + "_" + getTime(true) + ".jitmflog"
		var found = false
		Interceptor.attach(Module.getExportByName('libc.so', '[FUNCTION]'), {
			onEnter: function (args) {
				var fd = args[0].toInt32();

				if (Socket.type(fd) === null)
					return;
				if (Socket.type(fd).toString().indexOf('tcp') > -1) {
					var address = Socket.peerAddress(fd);
					if (address === null)
						return;
					found = true
				}
			},
			onLeave: function (args) {
				if (!found) {
					var time = getTime()
					var hookLog = time + ',' + SCENARIO + ',' + TYPE + '\n'
					if(online){
						getMessageObjects()
					}
					else{
						dumpHeap(SCENARIO)
					}
					if(complete){
						putInFile(hookLogName, hookLog)
						if(online){
							putInFile(jitmfLogName, jitmfLog)
						}else{
							putInFile(jitmfHeapLogName, jitmfHeapLog)
						}
					}
				}
				jitmfHeapLog = ''
				jitmfLog = ''
			}
		});
	});
});
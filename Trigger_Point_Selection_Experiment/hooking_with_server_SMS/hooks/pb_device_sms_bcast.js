var TAG_T = "[HOOK_TRACE]";
var TAG_L = "[EXECUTION_TRACE]";


function getTime()
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    return dateTime
}

function trace(pattern)
{
	var type = (pattern.toString().indexOf("!") === -1) ? "java" : "module";

	if (type === "module") {
		var res = new ApiResolver("module");
		var matches = res.enumerateMatchesSync(pattern);
		var targets = uniqBy(matches, JSON.stringify);
		targets.forEach(function(target) {
			traceModule(target.address, target.name);
		});

	} else if (type === "java") {
		var found = false;
		
		Java.enumerateLoadedClasses({
			onMatch: function(aClass) {
				console.log(aClass)
				if (aClass.startsWith(pattern)){
					console.log(aClass)
					
					traceClass(aClass);
				}
               
			},
			onComplete: function() {}
		});
		if (!found) {
			try {
				traceMethod(pattern);
			}
			catch(err) {}
		}
	}
}

function traceMethod(targetClassMethod)
{
	var Log = Java.use("android.util.Log");
	var delim = targetClassMethod.lastIndexOf(".");
	if (delim === -1) return;

	var targetClass = targetClassMethod.slice(0, delim)
	var targetMethod = targetClassMethod.slice(delim + 1, targetClassMethod.length)

    var hook = Java.use(targetClass);

    var overloadCount = 0
    try{
        overloadCount = hook[targetMethod].overloads.length;
    }
    catch(err){}
    console.log("Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n")
	Log.v(TAG_T, "Tracing " + targetClassMethod + " [" + overloadCount + " overload(s)]\n");
	
	if (targetClassMethod.startsWith('com.google.firebase.iid.FirebaseInstanceIdReceiver.onReceive')){

	}


	for (var i = 0; i < overloadCount; i++) {
        var test = hook[targetClassMethod]
		hook[targetMethod].overloads[i].implementation = function() {

			Java.perform(function() {
				console.log(Java.use("android.util.Log").getStackTraceString(Java.use("java.lang.Exception").$new()))
			});
		
            var out = '{"time": '+getTime()+', "method": '+targetClassMethod+','
		
			for (var j = 0; j < arguments.length; j++) {
                var term_char = ', '
                var arg = arguments[j];
                
				out += '"arg[' + j + ']": ' + arguments[j]+term_char
			}

			var retval = this[targetMethod].apply(this, arguments);
			out += '"retval": ' + retval +'}\n'
			console.error(out);
			Log.v(TAG_L, out);
			return retval;
		}
	}
}

function Where(stack){
	var at = ""
	for(var i = 0; i < stack.length; ++i){
		at += stack[i].toString() + "\n"
	}
	return at
}

function uniqBy(array, key)
{
        var seen = {};
        return array.filter(function(item) {
                var k = key(item);
                return seen.hasOwnProperty(k) ? false : (seen[k] = true);
        });
}

function traceClass(targetClass) {
    var hook = Java.use(targetClass);
	var methods = hook.class.getDeclaredMethods();
	var parsedMethods = [];
	methods.forEach(function(method) {
		parsedMethods.push(method.toString().replace(targetClass + ".", "TOKEN").match(/\sTOKEN(.*)\(/)[1]);
	});

	var targets = uniqBy(parsedMethods, JSON.stringify);
	targets.forEach(function(targetMethod) {
		try{
		traceMethod(targetClass + "." + targetMethod);
		}
		catch(err){}
	});
}

setTimeout(function () {

Java.perform(function(){
		trace('com.pushbullet.android.JITMFSMSRcvEvent')
})
}, 0);
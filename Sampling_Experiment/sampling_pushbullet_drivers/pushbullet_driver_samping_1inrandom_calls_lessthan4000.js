var LOG_DIR = "jitmflogs";
var APP = "pushbullet";
var TYPE = "native";
var EVENT = "Pushbullet Message Present";
const trigger_points = ["write","read"];// "write" ->  SP trigger point | "read" -> CP trigger point

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var OWNER_PHONE_NUMBER = "+356 79247196"

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime;
}
function getExternalStorageDirectory() {
    const Env = Java.use("android.os.Environment")
    var external_storage_dir = Env.getExternalStorageDirectory().getAbsolutePath()
    return external_storage_dir
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function parseObject(instance,TP) {
    var json=instance.toString()    
    var object = '';
    var str1_pt1='{"type":"push",'
    var str1_pt2='"push":{"type":"sms_changed","source_device_iden":'    
    var str2_pt1='{"active":'
    var str2_pt2='"message":'
    
    if(json.includes(str1_pt1) && json.includes(str1_pt2) || json.includes(str2_pt1) && json.includes(str2_pt2) ){
        object = json;
    }    
    return object;
}

function getMessageObjects(time,TP) {
	var klass = "org.json.JSONObject"
	Java.choose(klass, {
		onMatch: function (instance) {
            var object = parseObject(instance,TP)
			if (object != '') { 
                jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": '
                jitmfLog += object + '}\n'
			}

		}, onComplete: function () {
			complete = true
		}
	});
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + TP + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(online){
        getMessageObjects(time, TP)
    } else {
        dumpHeap(TP)
    }
    if (complete) {
        putInFile(hookLogName, hookLog)
        if(online){
            putInFile(jitmfLogName, jitmfLog)
        } else {
            putInFile(jitmfHeapLogName, jitmfHeapLog)
        }
    }
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var hookLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = getExternalStorageDirectory() + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
        var predicateFlag = false;
        var samplingFlag = false;
        var dumpFlag = false;
     
        var rndCallNo = null;
        var counter = 0;

        var methods = trigger_points
        methods.forEach(function (TP) {
            Interceptor.attach(Module.getExportByName('libc.so', TP), {
                onEnter: function (args) {
                    samplingFlag = false;

                    // predicate for required for both READ and WRITE TP
                    var fd = args[0].toInt32();

                    if (Socket.type(fd) === null)
                        return;
                    if (Socket.type(fd).toString().indexOf('tcp') > -1) {
                        var address = Socket.peerAddress(fd);
                        if (address === null)
                            return;
                        predicateFlag = true
                    }

                    // sampling
                    counter = counter+1;
                    var rndInt = Math.floor(Math.random() * 4000)+1 //once every X amount of calls upper limit: 40; - new random number            

                    if(rndCallNo === null   || rndCallNo < counter){
                        rndCallNo = rndInt
                        counter = 0;
                    } else {  
                        if(counter == rndCallNo){
                            samplingFlag = true;
                            counter = 0;
                            rndCallNo = rndInt
                        }
                    }
                    dumpFlag = samplingFlag && predicateFlag;
                },
                onLeave: function (args) {
                    if (dumpFlag) {
                        dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP)
                    }
                    jitmfHeapLog = ''
                    jitmfLog = ''
                }
            });        
        });    
    });
});
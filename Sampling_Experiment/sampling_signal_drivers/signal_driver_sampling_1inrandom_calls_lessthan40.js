var LOG_DIR = "jitmflogs";
var APP = "signal";
var TYPE = "native";
var EVENT = "Signal Message Present";
const trigger_points = ["write","open"]; // "write" -> CP trigger point | "open" -> SP trigger point

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var owner_set=false
var owner_phone=''
var owner_id=''
var owner_username=''

var external_storage_dir = "";

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function getExternalStorageDirectory() {
    const Env = Java.use("androidx.core.content.ContextCompat");
    var currentApplication = Java.use('android.app.ActivityThread').currentApplication();
    var context = currentApplication.getApplicationContext();
    external_storage_dir = Env.getExternalFilesDirs(context,null)[0].getAbsolutePath();
}

function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null;
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function parseObject(instance) {
    var object = '';

    try {
        var messageObject = Java.cast(instance, Java.use("org.thoughtcrime.securesms.conversation.ConversationMessage"));
        var messageRecord = Java.cast(messageObject.messageRecord.value, Java.use("org.thoughtcrime.securesms.database.model.MessageRecord"));

        if(messageRecord  !== null){
            var text = messageRecord.body.value;            

            var to_username="";
            var to_id="";
            var to_phone="";

            var from_username="";
            var from_id="";
            var from_phone="";

            var recipient = Java.cast(messageRecord.individualRecipient.value, Java.use("org.thoughtcrime.securesms.recipients.Recipient"));          
            var recipientId = Java.cast(recipient.id.value, Java.use("org.thoughtcrime.securesms.recipients.RecipientId"));

            if (messageRecord.isOutgoing()) {
                
                to_username = recipient.username.value;
                to_phone = recipient.e164.value;
                to_id = recipientId;  

                from_username = owner_username;
                from_phone = owner_phone;
                from_id = owner_id;
            } else {
                from_username = recipient.username.value;
                from_phone = recipient.e164.value;
                from_id = recipientId;         
                
                to_username = owner_username;
                to_phone = owner_phone;
                to_id = owner_id;
            }
            
            object = '{"date": "' + messageRecord.dateSent.value + '", "message_id": "' + messageRecord.id.value + '", "text": "' + text + '", "to_id": "' + to_id+ '", "to_name": "' + to_username +'", "to_phone": "' + to_phone  + '", "from_id": "' + from_id+ '", "from_name": "' + from_username +'", "from_phone": "' + from_phone  + '"}';
        }       
    } catch (err) {}
    
    return object;
}

function getMessageObjects(time,TP) {
    var klass = "org.thoughtcrime.securesms.conversation.ConversationMessage";
    Java.choose(klass, {
        onMatch: function (instance) {
            var object = parseObject(instance);
            if (object != '') {
                jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": ';
                jitmfLog += object + '}\n';
            }

        }, onComplete: function () {
            complete = true;
        }
    });
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + TP + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(online){
        getMessageObjects(time, TP)
    } else {
        dumpHeap(TP)
    }
    if (complete) {
        putInFile(hookLogName, hookLog)
        if(online){
            putInFile(jitmfLogName, jitmfLog)
        } else {
            putInFile(jitmfHeapLogName, jitmfHeapLog)
        }
    }
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        var Log = Java.use("android.util.Log");
        getExternalStorageDirectory();
        var hookLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        var jitmfHeapLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";
        

        var rndCallNo = null;
        var counter = 0;
    
        var methods = trigger_points
        methods.forEach(function (TP) {
            Interceptor.attach(Module.getExportByName('libc.so', TP), {
                //no trigger predicate
                
                onLeave: function (args) {
                          var samplingFlag = false;
                    //setting variables before dump
                    if(!owner_set){
                        var Recipient = Java.use("org.thoughtcrime.securesms.recipients.Recipient");
                        var owner = Java.cast(Recipient.self(),Java.use("org.thoughtcrime.securesms.recipients.Recipient"))
                        if (owner !== undefined){
                            owner_set=true
                            owner_phone = owner.e164.value;
                            owner_username = owner.username.value;
                            var ownerId = Java.cast(owner.id.value, Java.use("org.thoughtcrime.securesms.recipients.RecipientId"));
                            owner_id = ownerId;
                        }
                    }

                    //sampling
                    counter = counter+1;
                    var rndInt = Math.floor(Math.random() * 40)+1 //once every X amount of calls upper limit: 40; - new random number            

                    if(rndCallNo === null   || rndCallNo < counter){
                        rndCallNo = rndInt
                        counter = 0;
                    } else {  
                        if(counter == rndCallNo){
                            samplingFlag = true;
                            counter = 0;
                            rndCallNo = rndInt
                        }
                    }
                  
                    if (samplingFlag) {
                        dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP)
                    }                                 
                        
                    jitmfHeapLog = ''
                    jitmfLog = ''
                }
            });        
        });        
    });
});
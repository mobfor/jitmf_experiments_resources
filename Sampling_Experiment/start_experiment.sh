#!/bin/bash

# to run: ./exp3.sh [ physical_phone | emu]

GLOBAL_LOOP=5
MSG_LOOP=10
IN_MSG_W8=5

results_folder_name="./results/results_GLOBAL_LOOP_${GLOBAL_LOOP}_MSG_LOOP_${MSG_LOOP}_IN_MSG_W8_${IN_MSG_W8}-`date +"%FT%T"`"
mkdir -p $results_folder_name
setup=$1

driver_cnt=`ls -l ./sampling_telegram_drivers/* | wc -l`
r=0

TELEGRAM_DRIVER_FILES="./sampling_telegram_drivers/*"
echo "Starting Telegram"
for f in $TELEGRAM_DRIVER_FILES
do
    let "r=r+1"

    for ((j=1; j <= $GLOBAL_LOOP; j++)); #overall loop
    do
        folder_name="`basename $f .js`_run$j"
        mkdir ./$results_folder_name/$folder_name

        echo "[**] >> Driver $r of $driver_cnt | RUN: $folder_name | Global loop $j of $GLOBAL_LOOP << "

        echo "Starting emulator"
        nohup ~/Android/Sdk/tools/emulator -avd exp3_jennifer -no-snapshot-save > /dev/null 2>&1 &
        sleep 5

        adb root
        adb shell dumpsys gfxinfo org.telegram.messenger reset 
        echo "Pushing the driver $f in /data/local/tmp/cp_tg_online_native_hook.js"
        adb push $f /data/local/tmp/cp_tg_online_native_hook.js 
        adb  shell am force-stop org.telegram.messenger # close app
        adb shell rm -rf /sdcard/jitmflogs/* 
        cd ./scripts\ for\ generating\ traffic/
        adb logcat -c

        sleep 2

        for ((i=1; i <= $MSG_LOOP; i++));
        do
            len=`shuf -i 10-100 -n 1`
            string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`

            wait_v=`shuf -i 0-$IN_MSG_W8 -n 1`
            echo "ROUND $i of $MSG_LOOP"
            echo "Sleeping for $wait_v"
            sleep $wait_v

            echo "Sending msg"
            ./telegram_usage_$setup.sh "ConfidentialInfo_$string" 
            current_date=`date "+%m-%d %T"`
            echo "$current_date V/[ACC_ATTACK_LOG]( 8430): Telegram, Sent message: ConfidentialInfo_$string " >> ../$results_folder_name/${folder_name}/ground_truth_timeline.txt
        done
        sleep 5

        cd ..

        adb shell dumpsys gfxinfo org.telegram.messenger | grep Janky >> ./$results_folder_name/$folder_name.txt 
        # adb shell find /sdcard/jitmflogs -type f -links 1 -printf "%s\n" | awk '{s=s+$1} END {print (s == "" ? 0 : s)}' >> ./$results_folder_name/$folder_name.txt #bytes
        adb logcat -d  -b crash -v time >> ./$results_folder_name/$folder_name.txt
        adb logcat -d -v time >> ./$results_folder_name/$folder_name/logcat.txt
        adb pull /sdcard/jitmflogs/ ./$results_folder_name/$folder_name/
        adb pull /data/anr/ ./$results_folder_name/$folder_name/ 
        adb shell rm -rf /sdcard/jitmflogs/*
        adb shell rm -rf /data/anr/*

        adb  shell am force-stop org.telegram.messenger 
        echo "Killing emulator"
        adb emu kill
        sleep 20
    done
done

driver_cnt=`ls -l ./sampling_signal_drivers/* | wc -l`
r=0

sleep 5
SIGNAL_DRIVER_FILES="./sampling_signal_drivers/*"

echo "Starting Signal"
for f in $SIGNAL_DRIVER_FILES
do
    let "r=r+1"

    for ((j=1; j <= $GLOBAL_LOOP; j++)); #overall loop
    do
        folder_name="`basename $f .js`_run$j"
        mkdir ./$results_folder_name/$folder_name

        echo "[**] >> Driver $r of $driver_cnt | RUN: $folder_name | Global loop $j of $GLOBAL_LOOP << "

        echo "Starting emulator"
        nohup ~/Android/Sdk/tools/emulator -avd exp3_jennifer -no-snapshot-save > /dev/null 2>&1 &
        sleep 5
            
        adb root
        adb shell dumpsys gfxinfo org.thoughtcrime.securesms reset 
        echo "Pushing the driver $f in /data/local/tmp/cp_signal_online_native_hook.js"
        adb push $f /data/local/tmp/cp_signal_online_native_hook.js 
        adb shell am force-stop org.thoughtcrime.securesms # close app
        adb shell rm -rf /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/* 
        cd ./scripts\ for\ generating\ traffic/
        adb logcat -c

        sleep 2

        for ((i=1; i <= $MSG_LOOP; i++));
        do
            len=`shuf -i 10-100 -n 1`
            string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`

            wait_v=`shuf -i 0-$IN_MSG_W8 -n 1`
            echo "ROUND $i of $MSG_LOOP"
            echo "Sleeping for $wait_v"
            sleep $wait_v

            echo "Sending msg"
            ./signal_usage_$setup.sh "ConfidentialInfo_$string" 
            current_date=`date "+%m-%d %T"`
            echo "$current_date V/[ACC_ATTACK_LOG]( 8430): Telegram, Sent message: ConfidentialInfo_$string " >> ../$results_folder_name/${folder_name}/ground_truth_timeline.txt
        done
        sleep 5

        cd ..

        adb shell dumpsys gfxinfo org.thoughtcrime.securesms | grep Janky >> ./$results_folder_name/$folder_name.txt 
        # adb shell find /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/ -type f -links 1 -printf "%s\n" | awk '{s=s+$1} END {print (s == "" ? 0 : s)}' >> ./$results_folder_name/$folder_name.txt #bytes
        adb logcat -d  -b crash -v time >> ./$results_folder_name/$folder_name.txt
        adb logcat -d -v time >> ./$results_folder_name/$folder_name/logcat.txt
        adb pull /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/ ./$results_folder_name/$folder_name/ 
        adb pull /data/anr/ ./$results_folder_name/$folder_name/
        adb shell rm -rf /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/*
        adb shell rm -rf /data/anr/*

        adb  shell am force-stop org.thoughtcrime.securesms 
        echo "Killing emulator"
        adb emu kill
        sleep 20
    done
done

driver_cnt=`ls -l ./sampling_pushbullet_drivers/* | wc -l`
r=0

sleep 5
PUSHBULLET_DRIVER_FILES="./sampling_pushbullet_drivers/*"

echo "Starting Pushbullet"
for f in $PUSHBULLET_DRIVER_FILES
do
    let "r=r+1"

    for ((j=1; j <= $GLOBAL_LOOP; j++)); #overall loop
    do
        folder_name="`basename $f .js`_run$j"
        mkdir ./$results_folder_name/$folder_name

        echo "[**] >> Driver $r of $driver_cnt | RUN: $folder_name | Global loop $j of $GLOBAL_LOOP << "
        
        echo "Starting emulator"
        nohup ~/Android/Sdk/tools/emulator -avd exp3_jennifer_pushbulletonly -no-snapshot-save > /dev/null 2>&1 &
        sleep 5

        adb root
        adb shell dumpsys gfxinfo com.pushbullet.android reset 
        echo "Pushing the driver $f in /data/local/tmp/cp_tg_online_native_hook.js"
        adb push $f /data/local/tmp/cp_pb_online_native_hook.js 
        adb shell am force-stop com.pushbullet.android # close app
        adb shell rm -rf /sdcard/jitmflogs/* 
        cd ./scripts\ for\ generating\ traffic/
        adb logcat -c

        sleep 2

        for ((i=1; i <= $MSG_LOOP; i++));
        do
            len=`shuf -i 10-100 -n 1`
            string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`

            wait_v=`shuf -i 0-$IN_MSG_W8 -n 1`
            echo "ROUND $i of $MSG_LOOP"
            echo "Sleeping for $wait_v"
            sleep $wait_v

            echo "Sending msg"
            ./pushbullet_usage_$setup.sh "ConfidentialInfo_$string"  ../$results_folder_name/${folder_name}/ground_truth_timeline.txt
        done
        sleep 5

        cd ..

        adb shell dumpsys gfxinfo com.pushbullet.android | grep Janky >> ./$results_folder_name/$folder_name.txt 
        # adb shell find /sdcard/jitmflogs -type f -links 1 -printf "%s\n" | awk '{s=s+$1} END {print (s == "" ? 0 : s)}' >> ./$results_folder_name/$folder_name.txt #bytes
        adb logcat -d  -b crash -v time >> ./$results_folder_name/$folder_name.txt
        adb logcat -d -v time >> ./$results_folder_name/$folder_name/logcat.txt
        adb pull /sdcard/jitmflogs/ ./$results_folder_name/$folder_name/ 
        adb pull /data/anr/ ./$results_folder_name/$folder_name/ 
        adb shell rm -rf /sdcard/jitmflogs/*
        adb shell rm -rf /data/anr/*


        adb  shell am force-stop orcom.pushbullet.android 
        echo "Killing emulator"
        adb emu kill
        sleep 20
    done
done

./normal_run.sh $results_folder_name/normal_run $setup $GLOBAL_LOOP $MSG_LOOP $IN_MSG_W8
# Contents of this Repository
`Determining_JIT-MF_Value_Experiment/*` -> This folder contains experiment scripts and resources for RQ1 \
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers and original) \
`   ├── automate_attack/*` -> Contains the scripts used in the experiment to simulate accessibility attack misuse. `_sp` indicate spying attack scenarios and `cp` indicate crime-proxy attack scenarios. For Pushbullet case study, spying occurs when remote Pushbullet screen is loaded in the attacker's browser. \
`   ├── docker_metasploit_env/*` -> Docker environment required for experiment setup to carry out Metasploit Accessibility Attacks\
`   ├── experiment_run/*` -> This folder contains experiment automation scripts \
`   │    ├── full_run_<app>_<attack_type>.sh` -> Scripts used to initiate experiment run for <app> and <attack_type>. These scripts insert JIT-MF driver, generate noise, simulate accessibility attack, gather baseline forensic artefacts, gather JIT-MF forensic artefacts and generate forensic timelines using Plaso. \
`   │    ├── create_kb.py` -> Generates a knowledge base from the sources gathered. \
`   │    └── phone_automation_scripts/*` -> Scripts used to automate noise generation on the phone.\
`   ├── git_repos/` -> Forked public repositories used for parsing forensic artefacts collected to create a knowledge base. Plaso was modfified to include a JIT-MF parser.\
`   │    ├── plaso` \
`   │    ├── signalbackup-tools` \
`   │    ├── teleparser` \
`   │    └── walitean ` \
`   ├── Dockerfile` -> Docker environment required for experiment setup. Initiates scripts from `experiment_run/*` \
`   └── startExpDocker.sh` -> Starts the docker container 

> **[JIT-MF version used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_2)**\
> **[JIT-MF drivers used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_2/resources/drivers)**\
> **[Processed Results](https://drive.google.com/drive/u/0/folders/1C8Ig63bqNmxmN3RORACEmy3X219dWbBw)**

\
`Trigger_Point_Selection_Experiment/*` -> This folder contains experiment scripts and resources for RQ2 part 1 \
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers and original) \
`   ├── hooking_with_server_IM` -> Backup copy of `hooking_with_server_IM_PENTEST`  \
`   ├── hooking_with_server_IM_PENTEST` -> Contains scripts used to run and process results for instant messaging case study (Telegram) \
`   │   ├── monkeyrunner` ->   Contains the scripts used in the experiment to simulate accessibility attack misuse. \
`   │   ├── hook_template.js` -> JIT-MF driver template, populated by variables in `invoke_exp_run.py`\
`   │   ├── hook_native.js` -> Equivalent to `hook_template.js` but used for native drivers.\
`   |   ├── invoke_exp_run.py` -> Starts experiment. Contains list of drivers, pipe-seperated as "event|trigger point type|function, to populate `hook_template.js` \
`   |   ├── post<-|_>.sh` -> Post-processing scripts\
`   |   └── res-agg.sh` -> Aggregates results\
`   ├── hooking_with_server_SMS` ->   Contains scripts used to run and process results for SMS case study (Pushbullet) \
`   │   ├── android_scripts` -> Contains the scripts used in the experiment to simulate accessibility attack misuse. Spying occurs when remote Pushbullet screen is loaded in the attacker's browser. \
`   │   ├── hook_template.js` -> JIT-MF driver template, populated by variables in `invoke_exp_run.py`\
`   │   ├── hook_native.js` ->  Equivalent to `hook_template.js` but used for native drivers.\
`   |   ├── invoke_exp_run.py` -> Starts experiment. Contains list of drivers, pipe-seperated as "event|trigger point type|function, to populate `hook_template.js`\
`   |   ├── post<-|_>.sh` -> Post-processing scripts\
`   |   └── time_console_res_agg.sh` -> Aggregates results\
`   └── pentest_tool_outputs` ->  \
> **[JIT-MF version used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_1)**  \
> **JIT-MF drivers used:** `Trigger_Point_Selection_Experiment/hooking_with_server_<IM_PENTEST/SMS>/hook_template.js` (in this repo. Specific trigger points used are listed in `invoke_exp_run.py`)\
> **[Results](https://drive.google.com/drive/u/0/folders/1rnJDKw98E6jUY-ITml4eJ8Z3R8L3iFYJ)**  

\
`Sampling_Experiment/*` -> This folder contains experiment scripts and resources for RQ2 part 2 \
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers and original) \
`   │    ├── Systematic_sampling_TP_limit_findout/*` -> Contains scripts used to find out the number of times a TP is hit - required for determining the required sample window size for an app when using Systematic Sampling.\
`   │    ├── sampling_<app>_drivers/*` -> These folders contain the JIT-MF drivers generated for app (pushbullet|signal|telegram), satering for every sampling window value and method. JIT-MF Drivers called "\*_1inrandom_lessthan\<X\>.js" are drivers implementing a periodic sampling approach. Those called "\*_1inrandom_calls_lessthan\<X\>.js" are drivers implementing a systematic sampling approach based on TP hits. X is the sample window value.\
`   │    └── scripts for generating traffic/*` -> Contains scripts used to automate typical app usage.\
`   ├── get_number_of_events_found.py` -> Gets the number of events found in a JIT-MF log file / number of events that occurred as per known ground truth.\
`   ├── start_experiment.sh` -> Script used to initiate experiment run. It pushes the JIT-MF driver, automates typical app usage, logs events in usage, collects JIT-MF forensic artifacts and gets number of crashed, Janky Frames and size of JIT-MF files accumulated on the phone.\
`   ├── process_findings.py` -> Processes output of `normal_run.sh` to output an excel sheet. \
`   ├── normal_run.sh` -> Same as `start_experiment.sh`, but without JIT-MF driver. \
`   └── process_normal_runs_findings.py` -> Same as `process_findings.py` for `normal_run.sh`

> **[JIT-MF version used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_3)** \
> **JIT-MF drivers used**: `Sampling_Experiment/sampling_<app>_drivers` (in this repo) \
> **[Raw Results](https://drive.google.com/drive/u/0/folders/1FSEnwn_WfL-ZD95Nv7CvoiJT_UWHHkL4)**  \
> **[Processed Results](https://docs.google.com/spreadsheets/d/1QzKePi33SLFW-5pwBazvDys4ns5trK3bzauSIJRvFag/edit#gid=1440632863)**  \

\
`Real_Attack_Experiment/*` -> This folder contains experiment scripts and resources for RQ3
`   ├── apps/*` -> Contains the apps used in the experiment (both instrumented with JIT-MF drivers, original and malicious app) \
`   ├── scripts_for_generating_traffic/*` ->   \
`   ├── whatsapp_driver/*` -> Contains the JIT-MF drivers used for this experiment  \
`   ├── abe.jar` -> Unpacks WhatApp backup files to produce a tar file from which the WhatsApp encryption key can be extracted.  \
`   ├── dumpsys_parser.py` -> Parser for `dumpsys` logs to be transformed in csv output to be ingested by Timesketch  \
`   ├── parse_csv_tools_to_timesketch.py` -> Parses output produced by Belkasoft, XRY to produce a csv out format that can be ingested by Timesketch  \
`   ├── pull_evidence.sh` -> Pulls evidence from forensic sources on Android phone.  \
`   ├── pull_evidence_android.sh` -> Same as `pull_evidence.sh`, required for Android 8 Device.  \
`   └── start_experiment.sh` -> Initiates the experiment. Initiates noise and stealthy removal of WhatsApp Pink messages. Pulls evidence and parses output. \

> **[JIT-MF version used](https://gitlab.com/bellj/jit-mf_framework/-/tree/experiment_3)** \
> **JIT-MF drivers used**: `Real_Attack_Experiment/whatsapp_driver` (in this repo) \
> **[Results](https://drive.google.com/drive/u/0/folders/1TrI8rfUZHH4mga3-jCdk3GMM3QVA3KJl)**:  
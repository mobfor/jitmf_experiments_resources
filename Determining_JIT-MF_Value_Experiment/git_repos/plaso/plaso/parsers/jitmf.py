#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Parser for teleparser (csv) files."""

import json
import glob

import codecs

from dfdatetime import posix_time as dfdatetime_posix_time
from dfdatetime import semantic_time as dfdatetime_semantic_time

from plaso.lib import errors
from plaso.lib import definitions
from plaso.containers import events
from plaso.containers import time_events
from plaso.parsers import interface
from plaso.parsers import manager


class JITMFEventData(events.EventData):

  """Android application usage event data.

  Attributes:
  package (str): name of the Android application.
  component (str): name of the individual component of the application.
  """

  DATA_TYPE = 'android:jitmf:event:time'

  def __init__(self):
    """Initializes event data."""

    super(JITMFEventData,
            self).__init__(data_type=self.DATA_TYPE)
    self.log_source = None
    self.trigger_point = None
    self.event = None
    self.content = None

class JITMFParser(interface.FileObjectParser):

  """Parses the Teleparser (csv) file."""

  NAME = 'jitmf'
  DATA_FORMAT = 'JITMF (json) file'
  _ENCODING = 'utf-8'

  def ParseFileObject(self, parser_mediator, file_object):
    """Parses an Android usage-history file-like object.
    Args:
    parser_mediator (ParserMediator): mediates interactions between parsers
    and other components, such as storage and dfvfs.
    file_object (dfvfs.FileIO): file-like object.

    Raises:
    UnableToParseFile: when the file cannot be parsed.
    """
    filename = parser_mediator.GetFilename()
    if filename != 'merged-logs-uniq.jitmflog': # cause there are the unprocessed ones as well
      raise errors.UnableToParseFile(
        'Not an Android usage history file [not XML]')

    file_content = file_object.read()
    file_content = codecs.decode(file_content, self._ENCODING)
    lines = file_content.splitlines()
    line_count = 0
    for row in lines:     
      line_count+=1
      data = json.loads(row)
      
      event_data = JITMFEventData()
      event_data.log_source = filename
      event_data.trigger_point = data['trigger_point']
      event_data.event = data['event']
      event_data.content = json.dumps(data['object'])
      # FOR DEBUGGING raise errors.UnableToParseFile('Not an Android usage history file [not XML]: '+event_data.log_source+'  :'+event_data.trigger_point+'  :'+event_data.event+'  :'+event_data.content)

      if len(row[0]) != 0:
        time_string = data['time']
        date_time = dfdatetime_posix_time.PosixTime(timestamp=time_string)
      else:
        parser_mediator.ProduceExtractionWarning((
            'Unable to parse start time string: {0:s} with error: No time available').format(row[0]))
        date_time = dfdatetime_semantic_time.InvalidTime()

      event = time_events.DateTimeValuesEvent(date_time,definitions.TIME_DESCRIPTION_START)
      parser_mediator.ProduceEventWithEventData(event, event_data)

# The current offset of the file-like object needs to point at
# the start of the file for ElementTree to parse the XML data correctly.

manager.ParsersManager.RegisterParser(JITMFParser)


#!/bin/bash
# $1 - parent dir to store results in 

j=$2 # full-run.sh will give "current" count
END_LOOP=$3 # full-run.sh will give number of "normal" runs
ATTACK_END_LOOP=$4 # full-run.sh will give number of attack runs
emulator=emulator-5554
scenario=signal_cp
PASSWORD=334239885113206703453090072869

echo "Starting: $scenario"

echo "Pushing the driver $JITMF_PATH/resources/drivers/cp_signal_online_native_hook.js.js"
adb push $JITMF_PATH/resources/drivers/cp_signal_online_native_hook.js /data/local/tmp/cp_signal_online_native_hook.js

echo "Stopping Signal"
adb shell am force-stop org.thoughtcrime.securesms

echo "Removing residue JITMF logs"
adb shell rm -rf /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/*

echo "Removing residue logcabt logs"
adb logcat -c

echo "Loop cnt: $z"

for ((i=1; i <= $END_LOOP; i++));
do
    let "j=j+1"
    echo "Generating Noise $i"
    len=`shuf -i 10-100 -n 1`
    string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`
    echo "Signal Attack"
    ./phone_automation_scripts/send_signal_msg_andstudio_pxl5.sh $emulator "Noise_$string"
    sleep 2
done

curr_dir=`pwd`
cd $ROOT_EXP_DIR/automate_attack

for ((i=1; i <= $ATTACK_END_LOOP; i++));
do
    wait_v=`shuf -i 10-20 -n 1`
    echo "Sleeping for $wait_v"
    sleep $wait_v
    
    echo "Generating Attack $i"
    let "j=j+1"
    ./activate_accessibility_payload_pxl5.sh $emulator
    ./run_attack.sh $scenario $j
    adb shell am force-stop org.thoughtcrime.securesms
done
cd $curr_dir

# Generate noise
for ((i=1; i <= $END_LOOP; i++));
do
    let "j=j+1"
    echo "Generating Noise $i"
    len=`shuf -i 10-100 -n 1`
    string=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c$len`
    echo "Signal Attack"
    ./phone_automation_scripts/send_signal_msg_andstudio_pxl5.sh $emulator "Noise_$string"
   
    sleep 2
done

mkdir -p $1
echo "Ground Truth - every 50 messages"
adb logcat -d -v time -s  "[ACC_ATTACK_LOG]" >> ${1}/ground_truth_timeline.txt

echo "Generate backup"
./phone_automation_scripts/generate_signal_backup_andstudio_pxl5.sh $emulator

# so as to get the logs at the same time
echo "Gathering baseline logs..."
./pull_files.sh $emulator $scenario $1/baseline
echo "Gathering baseline logs and JIT-MF sources..."
./pull_files.sh $emulator $scenario $1/enhanced process_jitmflogs

echo "Starting baseline timeline..."
# Generate Baseline timeline - using local/custom plaso
export PYTHONPATH=$PYTHONPATH:$ROOT_EXP_DIR/git_repos/plaso
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/log2timeline.py --parsers android $1/baseline/baselinse.plaso $1/baseline/data_extracted_msg_send/
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/psort.py -o L2tcsv $1/baseline/baselinse.plaso -w $1/baseline/supertimeline.csv

echo "Parsing baseline signal.db..."
mkdir $1/baseline/backup_database/signal_backup_decrypted
files=($1/baseline/backup_database/*)
backup_file="${files[0]}"
$ROOT_EXP_DIR/git_repos/signalbackup-tools/signalbackup-tools $backup_file $PASSWORD --output $1/baseline/backup_database/signal_backup_decrypted

# Generate Baseline timeline - putting all data in an sqlite table
python3 $ROOT_EXP_DIR/experiment_run/create_kb.py $1/baseline/ $scenario

echo "Cleaning JITMF logs..."
python3 $ROOT_EXP_DIR/experiment_run/post-parser.py $1/enhanced/data_extracted_msg_send/sdcard/files/jitmflogs $1/enhanced/data_extracted_msg_send $scenario

echo "Starting enhanced timeline..."
# Generate JITMF enhanced timeline - using local/custom plaso
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/log2timeline.py --parsers android $1/enhanced/enhanced.plaso $1/enhanced/data_extracted_msg_send/
python3 $ROOT_EXP_DIR/git_repos/plaso/tools/psort.py -o L2tcsv $1/enhanced/enhanced.plaso -w $1/enhanced/supertimeline.csv

echo "Parsing enhanced signal.db..."
mkdir $1/enhanced/backup_database/signal_backup_decrypted
files=($1/enhanced/backup_database/*)
backup_file="${files[0]}"
$ROOT_EXP_DIR/git_repos/signalbackup-tools/signalbackup-tools $backup_file $PASSWORD --output $1/enhanced/backup_database/signal_backup_decrypted

# Generate JITMF enhanced timeline - putting all data in an sqlite table
python3 $ROOT_EXP_DIR/experiment_run/create_kb.py $1/enhanced/ $scenario

echo "Remove any residue JIT-MF logs on the device..."
# Remove any residue JIT-MF logs on the device
adb shell rm -rf /sdcard/Android/data/org.thoughtcrime.securesms/files/jitmflogs/*
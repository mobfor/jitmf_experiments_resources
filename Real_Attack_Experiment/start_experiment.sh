#! /bin/bash

# ./exp3_pt3.sh

# Assumes:
# - 2 devices
# - whatsapp pink is installed and running
# - the two devices are contacts of one another 'uom_instrumented_1' and 'uom_phone_1'
# - there is an existing chat

export PATH=${PATH}:~/Android/Sdk/platform-tools/

ADBDEVICES=$(adb devices)
contact_emu=""
malicious_emu=""

while read line; do 
    vars=( $line )

    if [[ "${vars[1]}" = "device" ]]; then
        
        includes_malware=$(adb </dev/null -s ${vars[0]} shell pm list packages com.pik.pink)
        
        if [[ ! -z "$includes_malware" ]]; then
            malicious_emu="${vars[0]}"
        else
            contact_emu="${vars[0]}"
        fi
    fi
done <<< "$ADBDEVICES"

echo "Malicious emu: $malicious_emu"
echo "Contact emu: $contact_emu"

adb -s $malicious_emu shell rm -rf /sdcard/jitmflogs/*
# adb -s $malicious_emu shell rm -rf /storage/emulated/0/Android/data/com.whatsapp/files/jitmflogs/* #- FOR ANDROID 11
adb -s $malicious_emu shell rm -rf /sdcard/WhatsApp/*
adb -s $malicious_emu shell rm -rf /sdcard/com.whatsapp/*
adb -s $malicious_emu shell rm -rf /sdcard/shared_pref/*
adb -s $malicious_emu logcat -c
curr_dir=`pwd`
sleep 3
cd scripts_for_generating_traffic
python3 sending_whatsapp_msg_to_infected_phone.py "$contact_emu" 50 'uom_instrumented_1' &
sleep 30 
python3 deleting_whatsapp_pink_msg_to_me.py "$malicious_emu" 
sleep 10
adb -s $malicious_emu uninstall com.pik.pink
sleep 5
python3 sending_whatsapp_msg_to_infected_phone.py "$malicious_emu" 50 'uom_phone_1' &
cd $curr_dir

sleep 1800 #grab evidence after 30 minutes?
echo ">>> NOW <<<"
# ./pull_evidence.sh $1 $malicious_emu
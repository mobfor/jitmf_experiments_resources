var LOG_DIR = "jitmflogs";
var APP = "whatsapp";
var TYPE = "infra";
var EVENT = "Whatsapp Message Sent";
var trigger_point = "android.database.sqlite.SQLiteDatabase";

var jitmfHeapLog = ''
var jitmfLog = ''
var online=true
var complete=false

var external_storage_dir = "";

var Log = Java.use("android.util.Log");
var TAG_L = "[FRIDA_SCRIPT]";

function getTime(file) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = Math.round(today.getTime()/1000);
    if (file) {
        dateTime = date + '_' + time;
    }
    return dateTime
}

function changeAppSignature(){
    Log.v(TAG_L,"cHANGING SIGNATURE")
    var hook = Java.use("android.app.ApplicationPackageManager");
    var targetMethod="getPackageInfo"
    var overloadCount = hook[targetMethod].overloads.length;
    for (var i = 0; i < overloadCount; i++) {
        hook[targetMethod].overloads[i].implementation = function () {
            var retval = this[targetMethod].apply(this, arguments);
            if (arguments[0].includes("com.whatsapp")) {
                Log
                var JavaSignatureClass = Java.use('android.content.pm.Signature');

                var relevantSignature = ''
                relevantSignature = "30820332308202f0a00302010202044c2536a4300b06072a8648ce3804030500307c310b3009060355040613025553311330110603550408130a43616c69666f726e6961311430120603550407130b53616e746120436c61726131163014060355040a130d576861747341707020496e632e31143012060355040b130b456e67696e656572696e67311430120603550403130b427269616e204163746f6e301e170d3130303632353233303731365a170d3434303231353233303731365a307c310b3009060355040613025553311330110603550408130a43616c69666f726e6961311430120603550407130b53616e746120436c61726131163014060355040a130d576861747341707020496e632e31143012060355040b130b456e67696e656572696e67311430120603550403130b427269616e204163746f6e308201b83082012c06072a8648ce3804013082011f02818100fd7f53811d75122952df4a9c2eece4e7f611b7523cef4400c31e3f80b6512669455d402251fb593d8d58fabfc5f5ba30f6cb9b556cd7813b801d346ff26660b76b9950a5a49f9fe8047b1022c24fbba9d7feb7c61bf83b57e7c6a8a6150f04fb83f6d3c51ec3023554135a169132f675f3ae2b61d72aeff22203199dd14801c70215009760508f15230bccb292b982a2eb840bf0581cf502818100f7e1a085d69b3ddecbbcab5c36b857b97994afbbfa3aea82f9574c0b3d0782675159578ebad4594fe67107108180b449167123e84c281613b7cf09328cc8a6e13c167a8b547c8d28e0a3ae1e2bb3a675916ea37f0bfa213562f1fb627a01243bcca4f1bea8519089a883dfe15ae59f06928b665e807b552564014c3bfecf492a0381850002818100d1198b4b81687bcf246d41a8a725f0a989a51bce326e84c828e1f556648bd71da487054d6de70fff4b49432b6862aa48fc2a93161b2c15a2ff5e671672dfb576e9d12aaff7369b9a99d04fb29d2bbbb2a503ee41b1ff37887064f41fe2805609063500a8e547349282d15981cdb58a08bede51dd7e9867295b3dfb45ffc6b259300b06072a8648ce3804030500032f00302c021400a602a7477acf841077237be090df436582ca2f0214350ce0268d07e71e55774ab4eacd4d071cd1efad"

                var newSignature = JavaSignatureClass.$new(relevantSignature);

                retval.signatures.value = [newSignature]            
            }
            return retval
        }        
    }
}

function changeMD5hash(){
    Log.v(TAG_L,"cHANGING MD5")
    var hook = Java.use("X.053");
    var targetMethod="A0D"
    var overloadCount = hook[targetMethod].overloads.length;
    for (var i = 0; i < overloadCount; i++) {
        hook[targetMethod].overloads[i].implementation = function () {
            var retval = Java.array('byte', [-2, -36, -55, -92, -98, -59, -116, 51, 62, 110, -18, -127, -119, -91, -122, 35])
            return retval
        }        
    }
}

function getExternalStorageDirectory() {
    var currentApplication = Java.use('android.app.ActivityThread').currentApplication();
    var context = currentApplication.getApplicationContext();
    
    external_storage_dir = context.getExternalFilesDir(null).getAbsolutePath();
}


function putInFile(fileName, contents, mode) {
    mode = typeof mode !== 'undefined' ? mode : 'a+';
    try{
        var file = new File(fileName, mode);
    }
    catch (err) { 
        Log.v(TAG_L, "[*] file open err "+err)
    }
    if(mode === 'r'){
        try{
            var file = new File(fileName, mode);
            var br = Java.use("java.io.BufferedReader");
            var fr = Java.use("java.io.FileReader");

            var bufreader = br.$new(fr.$new(fileName)); //expecting a number
            return bufreader.readLine()
        }
        catch (err) { 
            return null; // random number will never be 0
        }
    }
    else{
        var file = new File(fileName, mode);
        file.write(contents)
        file.close()
    }
}

function dumpHeap(TP) {
    var memdump_filename = APP + '_' + TP + "_" + TYPE + "_" + getTime(true) + ".hprof"
    jitmfHeapLog += '{"time": "' + getTime() + '","event": "' +EVENT+'","trigger_point": "' + TP + '", "object": [' + memdump_filename + ']}\n'
    const Debug = Java.use("android.os.Debug")
    Debug.dumpHprofData(getExternalStorageDirectory() + "/" + LOG_DIR + "/" + memdump_filename);
}

function dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,TP){
    complete = false;
    var time = getTime();
    var hookLog = time + ',' + trigger_point + ',' + TYPE + '\n';

    hookLogName = hookLogName.replace("[TP]",TP);
    jitmfLogName = jitmfLogName.replace("[TP]",TP);
    jitmfHeapLogName = jitmfHeapLogName.replace("[TP]",TP);

    if(!online){
        dumpHeap(trigger_point)
    }
    
    putInFile(hookLogName, hookLog)
    if(online){
        putInFile(jitmfLogName, jitmfLog)
    } else {
        putInFile(jitmfHeapLogName, jitmfHeapLog)
    }
    
}

// check for Trigger Point  call
setImmediate(function () {
    Java.perform(function () {
        getExternalStorageDirectory();
        var hookLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".hooklog";
        Log.v(TAG_L,hookLogName)
        var jitmfHeapLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmfheaplog";
        var jitmfLogName = external_storage_dir + "/" + LOG_DIR + "/" + APP + "_[TP]_" + TYPE + "_" + getTime(true) + ".jitmflog";

        changeAppSignature();
        changeMD5hash();

        
        var sqliteDatabase = Java.use(trigger_point);
        var timestamp =""
        
        sqliteDatabase.update.overload('java.lang.String', 'android.content.ContentValues', 'java.lang.String', '[Ljava.lang.String;').implementation = function(var0, var1, var2, var3) {
            Log.v(TAG_L,"[*] IN")
            if (var1.containsKey("sort_timestamp"))
            {
                timestamp = var1.get("sort_timestamp")
            }
            var updateRes = this.update(var0, var1, var2, var3);
            return updateRes;
        };

        sqliteDatabase.insert.overload('java.lang.String', 'java.lang.String', 'android.content.ContentValues').implementation = function(var0, var1, var2) {
            try{    
                if (var2.containsKey("item")){
                    var item = var2.get("item") 
                    var time = getTime();
                    jitmfLog += '{"time": "' + time + '","event": "' +EVENT+'","trigger_point": "' + trigger_point + '", "object": "'
                    jitmfLog += item + '","time_of_message_event": "'+timestamp+'"}\n'

                    Log.v(TAG_L,jitmfLog)
                    dumpEvidence(hookLogName,jitmfLogName,jitmfHeapLogName,trigger_point)
                }
            }
            catch (err) { 
                Log.v(TAG_L,"eroor"+err)
            }
            var insertValueRes = this.insert(var0, var1, var2);
            return insertValueRes;
        };
        
    });
});

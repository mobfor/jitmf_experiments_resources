import re
import sys
import time
import os
import random
import string

from com.dtmilano.android.viewclient import ViewClient

def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

MAX_WAIT=10
_s = 3

device = sys.argv[1]
no_msgs = int(sys.argv[2])
contact_name = sys.argv[3]
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity(component='com.whatsapp/com.whatsapp.Main')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.dump(window=-1)
vc.sleep(_s)

vc.findViewWithTextOrRaise(contact_name).touch()
vc.dump(window=-1)
vc.sleep(_s)

for i in range(no_msgs):
    message_text=get_random_string(15)
    wait_between_messages=random.randint(1,MAX_WAIT)
    vc.findViewWithTextOrRaise(u'Type a message').setText(message_text)
    vc.sleep(_s)
    vc.dump(window=-1)
    vc.findViewWithContentDescriptionOrRaise(u'''Send''').touch()
    vc.sleep(_s)
    time.sleep(wait_between_messages)
    vc.dump(window=-1)

vc.dump(window=-1)
vc.findViewByIdOrRaise("com.whatsapp:id/back").touch()
device.shell('input keyevent KEYCODE_BACK')
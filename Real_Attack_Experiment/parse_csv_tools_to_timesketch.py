import json
import datetime as dt
from dateutil import parser
import base64
import os
import glob
import csv

def parse_jitmf(jitmf_path, outfile):
    with open(jitmf_path) as jitmf_json_file:
        line = jitmf_json_file.readline()
        while line:
            if len(line.strip()) > 0:                
                jitmflog_dict = json.loads(line)
                decoded_value = str(base64.b64decode(jitmflog_dict['evidence_object']['original_object']))
                
                datetime = dt.datetime.utcfromtimestamp(int(jitmflog_dict['evidence_object']['time_sent'])/1000.0).strftime('%Y-%m-%dT%H:%M:%S')

                if 'SendE2EMessageJob' in decoded_value:
                    message='JITMF Whatsapp Message Event | Outgoing Message "'+ jitmflog_dict['evidence_object']['text'] +'" contact:'+ jitmflog_dict['evidence_object']['number']
                elif 'SendReadReceiptJob' in decoded_value:
                    message='JITMF Whatsapp Message Event | Read Incoming Message'
                
                print_line=message+","+jitmflog_dict['evidence_object']['time_sent']+","+datetime+","+"Event time\n"
                outfile.write(print_line)
            line = jitmf_json_file.readline()

def parse_belkasoft(belkasoft_files,outfile):
    with open(belkasoft_files) as in_file:
        csv_file = csv.reader(in_file, delimiter=',')
        for row in csv_file:
            TIME_FORMAT="%d/%m/%Y %H:%M:%S"
            dtobj_utc = dt.datetime.strptime(row[7], TIME_FORMAT).replace(tzinfo=dt.timezone.utc)
            datetime = dtobj_utc.strftime('%Y-%m-%dT%H:%M:%S')
            timestamp = dtobj_utc.timestamp()
            
            message='Belkasoft Whatsapp Message Event | '+row[2]+' Message "'+ row[8] +'" from:'+ row[4]+'" to:'+ row[5]
                
            print_line=message+","+str(timestamp)+","+str(datetime)+","+"Event time\n"
            outfile.write(print_line)

def parse_xry(xry_files,outfile):
    with open(xry_files) as in_file:
        csv_file = csv.DictReader(in_file)
        
        for row in csv_file:
            TIME_FORMAT="%d/%m/%Y %H:%M:%S"

            time_array = row["Time"].split(" UTC")
            time_set = time_array[0]
            time_zone = time_array[1].split(" ")[0]

            formatted_date = dt.datetime.strptime(time_set, TIME_FORMAT)
            if len(time_zone) ==0: #already UTC
                formatted_date = formatted_date.replace(tzinfo=dt.timezone.utc)
                timestamp = formatted_date.timestamp()
                datetime = dt.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S')
            else:
                datetime_for_parse = str(formatted_date.strftime('%Y-%m-%dT%H:%M:%S'))+" "+time_zone
                tz_aware_dt_obj = parser.parse(datetime_for_parse)
                timestamp=tz_aware_dt_obj.timestamp()
                datetime = dt.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S')
            
            message='XRY Whatsapp Message Event | '+row["Direction"]+' Message "'+ row["Text"] +'" from: ['+ row["From"].replace('\n', ' ')+'"] to: ['+ row["To"].replace('\n', ' ')+']'
                
            print_line=message+","+str(timestamp)+","+str(datetime)+","+"Event time\n"
            outfile.write(print_line)

results_folder='/mnt/2TBdrive/gitlab/experiment_3/Real_Attack_Experiment/results'
devices_results=['nexus5_2021.10.11-21.20.29','pixel4_2021.10.09-10.31.43','samsungs21_2021.10.12-13.00.57']
jitmflog_relative_path='data_extracted_msg_send/jitmflogs/merged-logs_uniq_b64d.jitmflog'
timesketch_folder='timesketch'
xry_files="*xry.csv"
belkasoft_files="*belkasoft.csv"

for result in devices_results:
    jitmf_path = os.path.join(results_folder,result,jitmflog_relative_path)
    xry_path = glob.glob(os.path.join(results_folder,result,xry_files))[0]
    belkasoft_path = glob.glob(os.path.join(results_folder,result,belkasoft_files))[0]

    timesketch_path = os.path.join(results_folder,result,timesketch_folder)
    if not os.path.exists(timesketch_path):
        os.makedirs(timesketch_path)
    print("rm -rf "+timesketch_path+" && \\")

    # PARSE JITMF
    outfilename=os.path.join(timesketch_path,"jitmflogs.csv")
    outfile= open(outfilename,"w+")
    outfile.write("message,timestamp,datetime,timestamp_desc\n")
    parse_jitmf(jitmf_path,outfile)

    # PARSE BELKASOFT
    outfilename=os.path.join(timesketch_path,"belkasoft.csv")
    outfile= open(outfilename,"w+")
    outfile.write("message,timestamp,datetime,timestamp_desc\n")
    parse_belkasoft(belkasoft_path,outfile)

    # PARSE XRY
    outfilename=os.path.join(timesketch_path,"xry.csv")
    outfile= open(outfilename,"w+")
    outfile.write("message,timestamp,datetime,timestamp_desc\n")
    parse_xry(xry_path,outfile)